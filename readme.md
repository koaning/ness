# Never (Ever) Stop Stopping 

This repo has benchmarking scripts for a heuristic that 
I came up with during our internal deep learning training.

The main observation came from a profiler run.  

```bash
Hits         Time  Per Hit   % Time  Line Contents
==================================================
                                     def run_training(optimizer, scheduler, iterations=400, logger=logger):
   1        529.0    529.0      0.0      run_id = str(uuid.uuid4())[:6]
   1          2.0      2.0      0.0      num_iterations = 400
   1         98.0     98.0      0.0      loss_fn = torch.nn.MSELoss()
   1       1703.0   1703.0      0.0      iterations = tqdm.trange(iterations)
  51      30125.0    590.7      0.1      for t in iterations:
  50    2079023.0  41580.5      9.4          y_pred = fastfm_model(user_cat, item_cat)
  50       8553.0    171.1      0.0          loss = loss_fn(y_pred, ratings)
  50      10566.0    211.3      0.0          mae = torch.mean(torch.abs(y_pred - ratings))
  50      76284.0   1525.7      0.3          iterations.set_description(f'mse={loss:.4} mae={mae:.4}')
  50        101.0      2.0      0.0          if t % 2 == 0:
  25       1672.0     66.9      0.0              losses.append(loss.detach().numpy())
  25         48.0      1.9      0.0              lr = optimizer.param_groups[0]['lr']
  25         21.0      0.8      0.0              log_row(...)
  50      14293.0    285.9      0.1          optimizer.zero_grad()
  50   19776030.0 395520.6     89.7          loss.backward()
  50      29871.0    597.4      0.1          optimizer.step()
  50        720.0     14.4      0.0          scheduler.step()
```

In the run it became clear that 90% of the time you are evaluating
the gradient. Preventing calculating these gradients and stepping
in the right direction might be a good idea.

## First idea: Keep Stepping 

The heuristic is simple: keep walking in the direction of the gradient
but re-evaluate once we starting performing worse.

## Second idea: Stop Stepping 

You want to prevent taking that one step that causes overstepping. 

## Final idea: Never (Ever) Stop Stepping

Maybe you just want to make a single step with a good stepsize. 
Newton to the rescue! 