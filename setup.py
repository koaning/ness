from setuptools import setup, find_packages

requirements = ['torch==1.0.0',
                'pandas==0.23.4',
                'matplotlib==3.0.2',
                'plotnine==0.5.1',
                'tqdm==4.28.1']


setup(
    name='neverstop',
    version='0.0.1',
    packages=find_packages(),
    install_requires=requirements
)
