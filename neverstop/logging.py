import logging
import json
import datetime as dt


def setup_logger(filename='grid.json'):
    # Create a custom logger
    logger = logging.getLogger(__name__)

    # Create handlers
    f_handler = logging.FileHandler(filename)
    f_handler.setLevel(logging.DEBUG)

    # Create formatters and add it to handlers
    f_format = logging.Formatter('%(message)s')
    f_handler.setFormatter(f_format)

    # Add handlers to the logger
    logger.addHandler(f_handler)
    return logger


def log_row(logger, **kwargs):
    kwargs['timestamp'] = str(dt.datetime.now())
    logger.warning(f"{json.dumps(kwargs)}")
