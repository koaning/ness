import torch
import torch.nn as nn


class FastFM(nn.Module):
    def __init__(self, df, user_dim=30, item_dim=30, hidden=10):
        super().__init__()
        self.embedding_user = nn.Embedding(df.user_id.nunique() + 1, user_dim)
        self.embedding_item = nn.Embedding(df.item_id.nunique() + 1, item_dim)
        self.linear1 = nn.Linear(user_dim + item_dim, hidden*2)
        self.linear2 = nn.Linear(hidden*2, hidden)
        self.linear3 = nn.Linear(hidden, 1)

    def forward(self, x_user, x_item):
        embed_user = self.embedding_user(x_user)
        embed_item = self.embedding_item(x_item)
        together = torch.cat((embed_user, embed_item), 1)
        h = nn.functional.relu(self.linear1(together))
        h = nn.functional.relu(self.linear2(h))
        return self.linear3(h)


if __name__ == "__main__":
    print("ohmy")
