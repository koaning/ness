import uuid

import tqdm
import torch
import numpy as np
import pandas as pd

from neverstop.logging import log_row, setup_logger


def parse_movie_dataset(filepath="ml-100k/u.data"):
    df = pd.read_csv(filepath, sep='\t', names=['user_id', 'item_id', 'rating', 'ts'])
    user_cat = torch.LongTensor(df.user_id.values.astype(np.float32))
    item_cat = torch.LongTensor(df.item_id.values.astype(np.float32))
    ratings = torch.from_numpy(df.rating.values.astype(np.float32).reshape(df.shape[0], 1))
    return df, user_cat, item_cat, ratings


def run_movies_normal(filepath, logger, model, optimizer, scheduler, iterations=400):
    df, user_cat, item_cat, ratings = parse_movie_dataset(filepath=filepath)
    run_id = str(uuid.uuid4())[:6]
    loss_fn = torch.nn.MSELoss()
    iterations = tqdm.trange(iterations)
    for t in iterations:
        y_pred = model(user_cat, item_cat)

        loss = loss_fn(y_pred, ratings)
        mae = torch.mean(torch.abs(y_pred - ratings))
        iterations.set_description(f'mse={loss:.4} mae={mae:.4}')
        if t % 2 == 0:
            lr = optimizer.param_groups[0]['lr']
            log_row(logger=logger,
                    tactic="normal",
                    run=run_id,
                    learning_rate=lr,
                    iteration=t,
                    model=model.__class__.__name__,
                    scheduler=scheduler.__class__.__name__,
                    optimizer=optimizer.__class__.__name__,
                    mae=float(mae.detach().numpy()),
                    mse=float(loss.detach().numpy()))
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        scheduler.step()


def run_movies_ness(filepath, logger, model, optimizer, scheduler, iterations=400):
    df, user_cat, item_cat, ratings = parse_movie_dataset(filepath=filepath)
    run_id = str(uuid.uuid4())[:6]
    loss_fn = torch.nn.MSELoss()
    iterations = tqdm.trange(iterations)
    eval_gradients = True
    y_pred = model(user_cat, item_cat)
    prev_loss = loss_fn(y_pred, ratings)
    for t in iterations:
        y_pred = model(user_cat, item_cat)

        loss = loss_fn(y_pred, ratings)
        if loss > prev_loss:
            eval_gradients = True

        mae = torch.mean(torch.abs(y_pred - ratings))
        iterations.set_description(f'mse={loss:.4} mae={mae:.4}')

        if eval_gradients:
            optimizer.zero_grad()
            loss.backward()
            eval_gradients = False
        optimizer.step()
        scheduler.step()
        prev_loss = loss
        lr = optimizer.param_groups[0]['lr']
        log_row(logger=logger,
                tactic="ness",
                run=run_id,
                learning_rate=lr,
                iteration=t,
                model=model.__class__.__name__,
                scheduler=scheduler.__class__.__name__,
                optimizer=optimizer.__class__.__name__,
                mae=float(mae.detach().numpy()),
                mse=float(loss.detach().numpy()))