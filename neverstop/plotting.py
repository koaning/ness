import json
import plotnine as p9
import pandas as pd


def read_grid_data(filepath='grid.json'):
    with open(filepath) as f:
        pltr = pd.DataFrame([json.loads(line) for line in f])
    return pltr


def plot_losses(dataf):
    pltr = dataf.assign(group=lambda d: d.tactic + '-' + d.optimizer)
    return (p9.ggplot() +
            p9.ggtitle("loss function over iterations") +
            p9.theme(legend_position="bottom") +
            p9.geom_line(data=pltr,
                         mapping=p9.aes(x="iteration", y="mse",
                                        group="run", color="group")))
